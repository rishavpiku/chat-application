
# Web Based Chat Application

This is a Web based Chat-Application which has been written on Python and it uses Flask as a microframework. It has been deployed on Google Cloud and you could find the link below.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. I have also deployed the application on Google Cloud. 

### Clone the Repository

```
git clone https://rishavpiku@bitbucket.org/rishavpiku/chat-application.git
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
pip install -r requirements.txt

```
## Running the tests

Run the test cases using the command :
```
python2 main_test.py

```
## Open Browser - localhost:9090 (Output)

## Deploying the Application in Public Cloud 
We are going to deploy our app on the Google Cloud
We would use the command:
```
gcloud init

gcloud app deploy

```
We need to make sure that we are in the same directory with that of the app.yaml
## Refer to the Screenshot - 1.png for the output

Deployed service - https://chat-application-209312.appspot.com

## You can stream logs from the command line by running:
 
 
 gcloud app logs tail -s default

## To view your application in the web browser run

 gcloud app browse

## Admin Panel
Username- admin
Password-123

## Screenshots (link) 
https://drive.google.com/file/d/1LZzHN1Osws5YtWmevyN-9QuiHdr2SoFb/view?usp=sharing

## Author

Rishav Chatterjee


